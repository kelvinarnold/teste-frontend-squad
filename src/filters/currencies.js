
/**
 * Filter all currencies by name
 * @param {array} data - Currency List
 * @param {string} term - Term to search
 */
const filterCurrency = (data, search) => {
  if (search === '') {
    return data;
  }
  return data.filter(item =>
    item.name.toLowerCase().indexOf(search.toLowerCase()) !== -1 ||
    item.symbol.toLowerCase().indexOf(search.toLowerCase()) !== -1);
}

export {
  filterCurrency
}