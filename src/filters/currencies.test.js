import {filterCurrency} from './currencies';

describe('filterCurrencyByName', () => {
  const currencies = [{
    symbol: 'BTC',
    name: 'Bitcoin',
    price_usd: '1'
  },
  {
    symbol: 'ETH',
    name: 'Ethereum',
    price_usd: '1'
  },
  {
    symbol: 'XRP',
    name: 'XRP',
    price_usd: '1'
  }]
  test('return correct counts where are 1 matching letters', () => {
    const currencyMatchCount = filterCurrency(currencies, 'BTC');
    expect(currencyMatchCount.length).toBe(1);
  });
  test('return correct counts where there are 2 matching letters', () => {
    const currencyMatchCount = filterCurrency(currencies, 'T');
    expect(currencyMatchCount.length).toBe(2);
  });
  test('return no matches', () => {
    const currencyMatchCount = filterCurrency(currencies, 'NONE');
    expect(currencyMatchCount.length).toBe(0);
  });
});