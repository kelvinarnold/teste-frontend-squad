import { connect } from 'react-redux';
import CurrencyList from './CurrencyList';
import { fetchCurrency } from './../../actions/action-currency';
import { filterCurrency } from './../../filters/currencies';

const mapStateToProps = (state) => ({
  currencies: filterCurrency(state.currencies.list, state.currencies.search)
});

const CurrencyContainer = connect(
  mapStateToProps,
  {fetchCurrency}
)(CurrencyList);

export default CurrencyContainer;
