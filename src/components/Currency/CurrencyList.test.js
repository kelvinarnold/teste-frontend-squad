import React from 'react';
import { shallow } from "enzyme";
import { CurrencyListContainer, CurrencyList } from "./";
import { checkProps, findByDataTest, storeFactory } from './../../test/testUtils';
import { currenciesMock } from './../../mock';

const defaultProps = {
  currencies: currenciesMock,
  fetchCurrency: () => {}
};
const defaultStoreState = {};
/**
 * Factory fucntion to create a ShallowWrapper for Search Component
 * @function setup
 * @param {object} props - Component props especific to this setup
 * @param {any} state - Initial state for setup
 * @param {object} storeState = Initial state for the store
 * @returns {ShallowWrapper}
 */
const setup = (props={}, state=null, storeState={}) => {
  const setupProps = { ...defaultProps, ...props };
  const setupStoreState = {...storeState, ...defaultStoreState};
  const store = storeFactory(setupStoreState);
  const wrapper = shallow(
    <CurrencyListContainer
      store={store}/>)
      .find(CurrencyList)
      .dive()
      .setProps({
        ...setupProps
      });
  if (state) { wrapper.setState(state); }
  return wrapper;
}

test('render without crashing', () => {
  setup();
});

test('test component props', () => {
  checkProps(CurrencyList, defaultProps);
});

describe('If there are empty currencies list', () => {
  let wrapper = null;
  beforeEach(() => {
    wrapper = setup({ currencies: null })
  });
  test('render "loading..." screen while fetch currencies', () => {
    const loading = findByDataTest(wrapper, 'currencies-loading');
    expect(loading.length).toBe(1);
  });
});

describe('If there are not currency match', () => {
  let wrapper = null;
  beforeEach(() => {
    wrapper = setup({ currencies: [] })
  });
  test('render notfound component', () => {
    wrapper.setState({ search: '' });
    const notfound = findByDataTest(wrapper, 'currency-not-found');
    expect(notfound.length).toBe(1);
  });
});

describe('If there are currency list', () => {
  let wrapper = null;
  beforeEach(() => {
    wrapper = setup();
  });
  test('render without error', () => {
    const currency_list = findByDataTest(wrapper, 'currency-list');
    expect(currency_list.length).toBe(1);
  });
  test('render currencies by criteria', () => {
    const currency_item = findByDataTest(wrapper, 'currency-item');
    expect(currency_item.length).toBe(3);
  });
})

describe('redux props', () => {
  test('has "currencies" of state as prop', () => {
    const wrapper = setup();
    const currenciesProps = wrapper.instance().props.currencies;
    expect(currenciesProps.length).toBe(3);
  });
  test('has "fetchCurrency"  of state as prop', () => {
    const wrapper = setup();
    const fetchCurrencyProps = wrapper.instance().props.fetchCurrency;
    expect(fetchCurrencyProps).toBeInstanceOf(Function);
  });
});