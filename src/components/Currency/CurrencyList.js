import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { CurrencyItem, CurrencyNotfound, CurrencyLoading } from './CurrencyItem';
import { SearchContainer } from './../Search';
import { CURRENCY_PAGE_TITLE } from './../../constants';
/**
 * CurrencyList Component
 * @class
 * @return {Class} - Rendered Currency Component
 */
class Currency extends Component {
  componentDidMount() {
    this.props.fetchCurrency();
  }
  renderCurrencyItem() {
    const {currencies} = this.props;
    if (!currencies) {
      return <CurrencyLoading data-test="currencies-loading"/>
    }
    if (!currencies.length) {
      return <CurrencyNotfound data-test="currency-not-found"/>
    }
    return (
      <div className="row"
        data-test="currency-list">
        {
          currencies.map(currency => {
            return (
              <CurrencyItem
                data-test="currency-item"
                currency={currency}
                key={currency.symbol}>
              </CurrencyItem>
            )
          })
        }
      </div>
    )
  }
  render() {
    return (
      <div className="col-12"
        data-test="currencies-list">
        <h2 className="d-block mt-3 mb-3">{CURRENCY_PAGE_TITLE}</h2>
        <div className="row mt-4">
          <SearchContainer/>
        </div>
        {this.renderCurrencyItem()}
      </div>
    )
  }
}

Currency.propTypes = {
  currencies: PropTypes.arrayOf(
    PropTypes.shape({
      symbol: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      price_usd: PropTypes.string.isRequired
    }).isRequired
  ),
  fetchCurrency: PropTypes.func.isRequired
}

export default Currency;