import React from 'react';
import { shallow } from "enzyme";
import CurrencyItem from "./CurrencyItem";
import { checkProps } from './../../../test/testUtils';

const defaultProps = {
  currency: {
    symbol: 'USD',
    name: 'Dolar',
    proce_usd: '1'
  }
}

/**
 * Factory fucntion to create a ShallowWrapper for Search Component
 * @function setup
 * @param {object} props - Component props especific to this setup
 * @param {any} state - Initial state for setup
 * @returns {ShallowWrapper}
 */
const setup = (props={}, state=null) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<CurrencyItem {...setupProps}/>);
}

test('render without crashing', () => {
  setup();
});

test('test component props', () => {
  checkProps(CurrencyItem, defaultProps);
});
