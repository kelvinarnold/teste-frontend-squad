import React from 'react';
import { CURRENCY_NOT_FOUND_TEXT,
  CURRENCY_NOT_FOUND_HELP } from './../../../constants';
/**
 * Notfoudn Currency
 * @function
 * @returns {Function} - React Functional Component
 */
export default () => {
  return (
    <div className="row">
      <div className="col-12">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title text-danger"
              data-test="not-found-text">
              {CURRENCY_NOT_FOUND_TEXT}</h5>
            <h6 className="card-subtitle text-muted">
              {CURRENCY_NOT_FOUND_HELP}</h6>
          </div>
        </div>
      </div>
    </div>
  )
}
