import React from 'react';
/**
 * Currency Loading
 * @function
 * @returns {Function} - React Functional Component
 */
export default () => {
  return (
    <div className="row">
      <div className="col-12">
        <h6 className="mt-2 mb-2 text-muted">Loading...</h6>
      </div>
    </div>
  )
}
