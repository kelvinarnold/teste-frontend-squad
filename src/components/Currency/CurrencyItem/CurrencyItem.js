import PropTypes from 'prop-types';
import React, { Component } from 'react';
/**
 * Currency Item
 * @class
 * @returns {class}
 */
class CurrencyItem extends Component {
  state = {
    hover: false
  }
  hoverChange() {
    this.setState({
      hover: !this.state.hover
    });
  }
  render() {
    const {currency} = this.props;
    const {hover} = this.state;
    return (
      <div className="col-12 col-sm-6 col-md-4 col-xl-3 mb-3"
        onMouseEnter={this.hoverChange.bind(this)}
        onMouseLeave={this.hoverChange.bind(this)}>
        <div className={`card ${hover ? 'border-primary' : ''}`}>
          <div className="card-body">
            <h5 className="card-title">
              {currency.symbol}
              <small className="text-black-50 d-block">{currency.name}</small>
            </h5>
            <h6 className="card-subtitle text-muted">US$ {currency.price_usd}</h6>
          </div>
        </div>
      </div>
    )
  }
}

CurrencyItem.propTypes = {
  currency: PropTypes.object
}

export default CurrencyItem;
