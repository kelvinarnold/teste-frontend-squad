import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { SEARCH_LABEL,
  SEARCH_PLACEHOLDER } from './../../constants';

class Search extends Component {
  state = {
    search: ""
  };
  onChangeInput = (event) => {
    let {value} = event.target;
    this.setState({search: value});
    this.props.setFilter(value);
  }
  render() {
    return (
      <div className="col-12 col-lg-8 col-xl-6 mb-3">
        <div className="form-group">
          <label className="font-italic">{SEARCH_LABEL} {this.state.search}</label>
          <div className="position-relative">
            <input className="form-control pr-4"
              data-test="search-input"
              value={this.state.search}
              type="text"
              onChange={this.onChangeInput}
              placeholder={SEARCH_PLACEHOLDER}></input>
            <span className="position-absolute text-primary"
              style={{top: '.4em', right: '.5em'}}>
              <i className="fas fa-search"></i>
            </span>
          </div>
        </div>
      </div>
    )
  }
}

Search.propTypes = {
  setFilter: PropTypes.func.isRequired
}

export default Search;
