import React from 'react';
import { shallow } from "enzyme";
import { SearchContainer, Search } from './';
import { findByDataTest, checkProps, storeFactory } from './../../test/testUtils';

const defaultProps = {
  setFilter: () => {}
}
const defaultStoreState = {
  currencies: {
    list: null,
    search: ''
  }
}
/**
 * Factory fucntion to create a ShallowWrapper for Search Component
 * @function setup
 * @param {object} props - Component props especific to this setup
 * @param {any} state - Initial state for setup
 * @param {object} storeState = Initial state for the store
 * @returns {ShallowWrapper}
 */
const setup = (storeState={}) => {
  const setupStoreState = {...storeState, ...defaultStoreState};
  const store = storeFactory(setupStoreState);
  const wrapper = shallow(<SearchContainer store={store}/>).find(Search).dive();
  return wrapper;
}

test('render without crashing', () => {
  setup();
});

test('test component props', () => {
  checkProps(Search, defaultProps);
});

describe('test search-input component', () => {
  let wrapper = null;
  beforeEach(() => wrapper = setup());
  test('render search-input search', () => {
    const input = findByDataTest(wrapper, 'search-input');
    expect(input.length).toBe(1);
  });
  test('set search-input text', () => {
    findByDataTest(wrapper, 'search-input')
    .simulate('change', {
      target: { value: 'demo' }
    });
    expect(findByDataTest(wrapper, 'search-input').props().value)
    .toEqual('demo');
  });
});

describe('test component states', () => {
  test('initial state value', () => {
    const wrapper = setup();
    const initialState = wrapper.state('search');
    expect(initialState).toBe("");
  });
});