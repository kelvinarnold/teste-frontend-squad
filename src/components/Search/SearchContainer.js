import { connect } from 'react-redux';
import { setFilter } from './../../actions/action-currency';
import Search from './Search';

const SearchContainer = connect(
  null,
  {setFilter}
)(Search);

export default SearchContainer;
