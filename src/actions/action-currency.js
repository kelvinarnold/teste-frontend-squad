// Import all Action Type
import { FETCH_CURRENCY, SET_FILTER } from './action-types';
import { HTTP } from './../service/http';
/**
 * @function fetchCurrency
 * @returns {object} - Action Object with type `FETCH_CURRENCY`
 */
const fetchCurrency = () => {
  const req = HTTP.get(`/ticker`);
  return {
    type: FETCH_CURRENCY,
    payload: req
  }
};
/**
 * @function setFilter
 * @returns {object} - Action Object with type `SET_FILTER`
 */
const setFilter = (term) => ({
  type: SET_FILTER,
  payload: term
})

// Export All Actions
export {
  fetchCurrency,
  setFilter
};