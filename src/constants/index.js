// App
const APP_NAME = 'Squad Test';
// Notfound page
const NOT_FOUND_MESSAGE = '404 not found page';
// Currency Constants
const CURRENCY_PAGE_TITLE = 'Criptomoedas no mercado';
const CURRENCY_NOT_FOUND_TEXT = 'Não foram encontradas ocurrencias';
const CURRENCY_NOT_FOUND_HELP = 'Tente com outro nome';
// Search Constants
const SEARCH_LABEL = 'Buscar Cryptomoeda';
const SEARCH_PLACEHOLDER ='Digite aqui o nome da moeda';

export {
  APP_NAME,
  NOT_FOUND_MESSAGE,
  CURRENCY_PAGE_TITLE,
  CURRENCY_NOT_FOUND_TEXT,
  CURRENCY_NOT_FOUND_HELP,
  SEARCH_LABEL,
  SEARCH_PLACEHOLDER
}