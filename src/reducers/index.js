import { combineReducers } from 'redux';
// Import all reducers here
import currencyReducer from './reducer-currency';

const appReducer = combineReducers({
  currencies: currencyReducer
})

export default appReducer;