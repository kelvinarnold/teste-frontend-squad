import reducerCurrency from './reducer-currency';
import { FETCH_CURRENCY, SET_FILTER } from './../actions/action-types';
import { currenciesMock } from './../mock';

describe('reducer-currency', () => {
  const currencies = currenciesMock;
  const searchFilter = 'BTC';
  test('should return initial defualt state', () => {
    const reducer = reducerCurrency();
    expect(reducer).toEqual({
      list: null,
      search: ''
    })
  });
  test('should return a list of currencies, actiontype `FETCH_CURRENCY` ', () => {
    const reducer = reducerCurrency(undefined, {
      type: FETCH_CURRENCY,
      payload: {
        data: currencies
      }
    });
    expect(reducer).toEqual({
      list: currencies,
      search: ''
    })
  });
  test('should set the filter state, actiontype `SET_FILTER` ', () => {
    const reducer = reducerCurrency(undefined, {
      type: SET_FILTER,
      payload: searchFilter
    });
    expect(reducer).toEqual({
      list: null,
      search: searchFilter
    })
  });
});