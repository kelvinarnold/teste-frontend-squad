import axios from 'axios';

const API = 'https://api.coinmarketcap.com/v1';

const config = {
  baseURL: API,
  headers: {
    'Content-Type': 'application/json',
  },
}
// Create new Axios Instance
const HTTP = axios.create(config);
export {
  HTTP
};