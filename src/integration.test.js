import moxios from 'moxios';
import { HTTP } from './service/http';
import { storeFactory } from './test/testUtils';
import { fetchCurrency, setFilter } from './actions/action-currency';
import { currenciesMock } from './mock';

describe('test fetchcurrency', () => {
  beforeEach(() => moxios.install(HTTP));
  afterEach(() => moxios.uninstall(HTTP) );
  const initialState = {
    currencies: {
      list: null
    } 
  }
  const store = storeFactory(initialState);
  test('get all currencies', () => {
    const payload = currenciesMock;
    moxios.wait(() => {
      let requests = moxios.requests.mostRecent();
      requests.respondWith({
        status: 200,
        response: payload,
      });
    });

    return store.dispatch(fetchCurrency())
    .then(() => {
      const newState = store.getState();
      const expectedState = {
        ...initialState,
        currencies: {
          list: payload
        }
      }
      expect(newState).toEqual(expectedState);
    })
  });
});

describe('test setfilter', () => {
  const search = 'BTC';
  const initialState = {
    currencies: {
      search
    } 
  };
  const store = storeFactory(initialState);
  test('get some currencies', () => {
    store.dispatch(setFilter(search));
    const newState = store.getState();
    const expectedState = {
      ...initialState,
      currencies: {
        search: search
      }
    };
    expect(newState).toEqual(expectedState);
  });
});