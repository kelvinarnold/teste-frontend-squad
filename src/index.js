import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import configStore from './configStore';

// Import Components
import { App } from './components/App';

ReactDOM.render(
  <Provider
    store={configStore}>
    <App/>
  </Provider>
,document.getElementById('root'));
