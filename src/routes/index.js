import React from 'react';
import { Route, Switch } from 'react-router-dom';
// Imort Views
import { CurrencyListContainer } from './../components/Currency';
import { NotFound } from './../components/NotFound';

const Routes = () => {
  return (
    <div className="row">
      <div className="d-flex w-100 justify-content-center pt-3 pb-3">
        <div className="col-12 col-lg-10 p-0 mb-4">
          <Switch>
            <Route exact path="/" component={CurrencyListContainer}></Route>
            <Route component={NotFound}/>
          </Switch>
        </div>
      </div>
    </div>
  )
}

export default Routes;
