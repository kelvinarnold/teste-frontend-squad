const currenciesMock = [
  {
    symbol: 'BTC',
    name: 'Bitcoin',
    price_usd: '1'
  },
  {
    symbol: 'ETH',
    name: 'Ethereum',
    price_usd: '1'
  },
  {
    symbol: 'XRP',
    name: 'XRP',
    price_usd: '1'
  }
];

export default currenciesMock;