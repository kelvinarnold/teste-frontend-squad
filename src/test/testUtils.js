import CheckPropTypes from 'check-prop-types';
import { createStore, applyMiddleware } from 'redux';
import ReduxPromise from 'redux-promise';
import appReducer from './../reducers';
/**
 * Return ShallowWrapper contiane node(s) with contain [data-test] value
 * @function findByDataTest
 * @param {ShallowWrapper}  wrapper - Enzyne ShallowWrapper
 * @param {string} val - Value of the [data-test]
 */
const findByDataTest = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);
/**
 * Check Props Types
 * @function checkProps
 * @param {*} component - Component
 * @param {*} conformingProps - All props to verify
 */
const checkProps = (component, conformingProps) => {
  const propError = CheckPropTypes(
    // eslint-disable-next-line
    component.propTypes,
    conformingProps,
    'prop',
    component.name
  )
  expect(propError).toBeUndefined();
}
/**
 * Create a test store with imported reducers
 * @function storeFactory
 * @param {object} initialState - Initial state for the store
 * @returns {Store} - Redux store
 */
const storeFactory = (initialState) => {
  // eslint-disable-next-line
  const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore)
  return createStoreWithMiddleware(appReducer, initialState);
}
export {
  findByDataTest,
  checkProps,
  storeFactory
}
